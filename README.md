# language implementation of Scheme for zamba

This repository assumes the be cloned by zamba, so it lies within the correct position on your file system.
The Makefile is mutated when called from zamba.

## zamba version

`0.8.0`
