;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(##namespace ("ts--scheme#"
parent-scopes
imported-namespaces
filter-completions
))

;; propagate the associated filetypes.
(set! languages-by-suffix
  (append languages-by-suffix
          '((".scm" . "scheme")
            (".ss" . "scheme"))))

;; add the own language-object of ts
(set! known-languages
  (cons (cons "scheme" (ts--scheme#language-new))
        known-languages))

(define parent-scopes
  '(("body" local)
    ("binding" local)
    ("program" member)))

;; language dependend
(define keywords '())

(define (imported-namespaces uri doc)
  (let* ((ts--imports (read-file-relative-to (this-source-file) "queries/imports.ss"))
         (imports
           (fold (lambda (imp acc)
                   (let ((full (read-node (lsp-document-content doc) (cadr (member "name" imp)))))
                     (cons full acc)))
                 '()
                 (ts--query-collect! doc ts--imports (ts--tree-root-node (lsp-document-tree doc))))))
    (fold (lambda (it acc) (if (member it acc) acc (cons it acc))) '() imports)))

(define (filter-completions item-vector isIncomplete-box cursor uri doc word current-node)
  (vector-append
    ;; append the keywords.
    (list->vector
      (fold
        (lambda (keyword acc)
          (if (>= (string-length keyword) strlen)
              (if (string=? (substring keyword 0 strlen) search-string)
                  (cons `(("label" . ,keyword)
                          ("kind" . ,completion-item-kind-keyword)
                          ("detail" . "Scheme Syntax")) acc)
                  acc)
              acc))
        '()
        keywords))
    item-vector))
