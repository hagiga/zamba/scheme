# buildfile for the scheme language implementation of java.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-ltree-sitter -ltree-sitter-scheme"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/ts-scheme-binding.o1 ts-scheme-binding.scm
