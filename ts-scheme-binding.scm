;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(c-declare "#include <tree_sitter/api.h>
extern const TSLanguage *tree_sitter_scheme(void);")

(##namespace ("ts--scheme#"
language-new
))

;; the tree-sitter parser from the build library.
(define language-new (c-lambda () (pointer (struct "TSLanguage")) "___result_voidstar = (TSLanguage *) tree_sitter_scheme();"))
